# Chellow (E-Commerce Web App)

### Live Demo:

https://chellow.vercel.app/

## Setting Up Development Environment

#### 1. Cloning

Requirements: Git (https://git-scm.com/downloads)

Clone the repository to your local machine by entering the following in your cmd or terminal

```
git clone https://gitlab.com/zuitt-projects-chif/b123/capstone3-chifamba.git
```

#### 2. Installing Dependencies

Requirements: Node/NPM (https://nodejs.org/en/download/)

Install the dependencies locally using

```
npm install
```

#### 3. Setting Environment Variables

Create a `.env` file in the root folder (not src). Define the following

```
REACT_APP_API_URL = your_api_url
REACT_APP_CLOUDINARY_CLOUD_NAME=your_cloud_name
REACT_APP_CLOUDINARY_PRESET=your_cloudinary_preset
```

`REACT_APP_API_URL` - this is the path to your local or hosted API. Typically if you run it locally, it would be http://localhost:4000
The repository for the compatible API is found here: https://gitlab.com/zuitt-projects-chif/b123/capstone2-chifamba/

`REACT_APP_CLOUDINARY_CLOUD_NAME` - this is your cloudinary cloud name. To setup a cloudinary account see: https://cloudinary.com/documentation/how_to_integrate_cloudinary

`REACT_APP_CLOUDINARY_PRESET` - this is the upload preset. To set this up see https://cloudinary.com/documentation/upload_presets

#### 4. Starting development server

Start the development server using the following in your terminal:

```
npm start
```

Development server will start on http://localhost:3000

## Building for Production

You can build for production using:

```
npm run build
```
