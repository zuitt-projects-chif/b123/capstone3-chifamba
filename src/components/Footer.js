import React from 'react';
import { Col, Row } from 'react-bootstrap';
import {
  faFacebook,
  faGithub,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import constants from '../constants';

export default function Footer() {
  return (
    <footer className="text-center bg-secondary text-white py-3 px-4">
      <Row className="text-center ">
        <Col>
          {/* <p>
            {' '}
            <small>
              <a
                href="https://www.vecteezy.com/free-vector/cartoon-chef"
                className="text-white"
              >
                Cartoon Chef Vectors by Vecteezy
              </a>
            </small>
          </p> */}
          <p className="mb-0 pb-0">
            &copy;{' '}
            <a
              href="https://github.com/nadchif"
              target="_blank"
              rel="noreferrer"
              className="text-white"
            >
              Daniel Chifamba{' '}
            </a>{' '}
            &bull;
            <a
              className="btn btn-link text-white"
              href="https://www.websitepolicies.com/uploads/docs/privacy-policy-template.pdf"
              target="_blank"
              rel="noreferrer"
            >
              {constants.messages.PRIVACY_POLICY}
            </a>
            &bull;{' '}
            <a
              className="btn btn-link text-white"
              href="https://github.com/nadchif"
              target="_blank"
              rel="noreferrer"
            >
              {constants.messages.ABOUT_US}
            </a>
          </p>

          <ul className="list-inline text-nowrap my-0">
            <li className="list-inline-item">
              <a
                className="btn btn-link text-white"
                href="https://facebook.com"
                target="_blank"
                rel="noreferrer"
              >
                <FontAwesomeIcon icon={faFacebook} />
              </a>
            </li>
            <li className="list-inline-item">
              <a
                className="btn btn-link text-white"
                href="https://twitter.com/dchif"
                target="_blank"
                rel="noreferrer"
              >
                <FontAwesomeIcon icon={faTwitter} />
              </a>
            </li>
            <li className="list-inline-item">
              <a
                className="btn btn-link text-white"
                href="https://github.com/nadchif"
                target="_blank"
                rel="noreferrer"
              >
                <FontAwesomeIcon icon={faGithub} />
              </a>
            </li>
          </ul>

          <p className="mt-0 pt-0">
            <small>
              Disclaimer: This is an activity for fulfilling an assignment
            </small>
          </p>
        </Col>
      </Row>
    </footer>
  );
}
